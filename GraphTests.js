/**/

import {TestKit} from "./TestKit.js";
import {Graph} from "./Graph.js";

export class GraphTests {
    // region crux

    constructor() {
        this.kit = new TestKit();
    }

    * run() {
        // build list of tests, run the tests
        this.define();
        yield* this.kit.run();
    }

    // endregion crux


    // region tests

    define() {
        this.kit.group("Graph",
            () => {
                // region addVertex()

                this.kit.test("addVertex",
                    "When vertex doesn't exist yet, it is added at its sort point in the vertex and edge arrays.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        graph.vertices = ["b", "c", "d", "e"];
                        graph.edges = [[6, 4, 2], [6, 2], [7, 3, 8], [2]];

                        let name = "a";
                        let expVertices = ["a", "b", "c", "d", "e"];
                        let expEdges = [[], [6, 4, 2], [6, 2], [7, 3, 8], [2]];

                        //* act *//
                        graph.addVertex(name);

                        //* assert *//
                        // if vertex added, these should be the same
                        this.kit.equate(expVertices.length, graph.vertices.length);
                        this.kit.equate(expVertices.length, graph.edges.length);

                        // if vertex added, should appear at end, others unchanged;
                        // empty edges array should also be added for the vertex
                        for (let of = 0; of < expVertices.length; of++) {
                            // expecteds
                            let expVertex = expVertices[of];
                            let expEdge = expEdges[of];

                            // actuals
                            let acVertex = graph.vertices[of];
                            let acEdge = graph.edges[of];

                            // comparing
                            this.kit.equate(expVertex, acVertex);
                            this.kit.equate(expEdge.length, acEdge.length);
                        }
                    }
                );

                this.kit.test("addVertex",
                    "When vertex does already exist, the vertex and edge arrays are not changed.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        graph.vertices = ["b", "c", "d", "e"];
                        graph.edges = [[3, 2], [4, 1, 8], [6], []];

                        let name = "c";
                        let expVertices = ["b", "c", "d", "e"];
                        let expEdges = [[3, 2], [4, 1, 8], [6], []];

                        //* act *//
                        graph.addVertex(name);

                        //* assert *//
                        // if vertex added, these should be the same
                        this.kit.equate(expVertices.length, graph.vertices.length);
                        this.kit.equate(expVertices.length, graph.edges.length);

                        // if vertex exists and so not added, vertices should
                        // be unchanged, and so should the edges array
                        for (let of = 0; of < expVertices.length; of++) {
                            // expecteds
                            let expVertex = expVertices[of];
                            let expEdge = expEdges[of];

                            // actuals
                            let acVertex = graph.vertices[of];
                            let acEdge = graph.edges[of];

                            // comparing
                            this.kit.equate(expVertex, acVertex);
                            this.kit.equate(expEdge.length, acEdge.length);
                        }
                    }
                );

                this.kit.test("addVertex",
                    "When the vertex array is empty, the new vertex is added as its sole element.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        graph.vertices = [];
                        let name = "a";

                        let expVertices = ["a"];
                        let expEdges = [[]];

                        //* act *//
                        graph.addVertex(name);

                        //* assemble *//
                        let acVertices = graph.vertices;
                        let acEdges = graph.edges;

                        //* assert *//
                        // if vertex added, both these lengths should be right
                        this.kit.equate(expVertices.length, acVertices.length);
                        this.kit.equate(expEdges.length, acEdges.length);

                        // vertex added should have right name
                        this.kit.equate(expVertices[0], acVertices[0]);

                        // empty edges array should have been added
                        this.kit.equate(expEdges[0].length, graph.edges[0].length);
                    }
                );

                this.kit.test("addVertex",
                "When a vertex is added, any edge to a same- or higher-index vertex is incremented by one.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph, "a", "b", "d", "e");
                        graph.addEdge("a", "d");
                        graph.addEdge("a", "e");
                        graph.addEdge("d", "e");

                        // these are the "after" indices for far edges;
                        // before, they are [2, 3], [0, 3], and [0, 2]
                        let expAtA = [3, 4];
                        let expAtD = [0, 4];
                        let expAtE = [0, 3];

                        //* act *//
                        graph.addVertex("c");

                        //* assert *//
                        this.equateArrays(expAtA, graph.edges[0]);
                        this.equateArrays(expAtD, graph.edges[3]);
                        this.equateArrays(expAtE, graph.edges[4]);
                    }
                );

                this.kit.test("addVertex",
                "When a vertex is added at the end, existing edges are not changed.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph, "a", "b", "c", "d");
                        graph.addEdge("a", "d");
                        graph.addEdge("a", "b");
                        graph.addEdge("b", "c");
                        graph.addEdge("b", "d");

                        // these are the "after" arrays, but except for e's,
                        // which doesn't exist before vertex e is added,
                        // they whould be the same before and after.
                        let expAtA = [1, 3];
                        let expAtB = [0, 2, 3];
                        let expAtC = [1];
                        let expAtD = [0, 1];
                        let expAtE = [];

                        //* act *//
                        graph.addVertex("e");

                        //* assert *//
                        this.equateArrays(expAtA, graph.edges[0]);
                        this.equateArrays(expAtB, graph.edges[1]);
                        this.equateArrays(expAtC, graph.edges[2]);
                        this.equateArrays(expAtD, graph.edges[3]);
                        this.equateArrays(expAtE, graph.edges[4]);
                    }
                );

                // endregion addVertex()

                // region addEdge()

                this.kit.test("addEdge",
                    "When no edges exist, a new edge is added as the sole element of each vertex.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph, "f", "g", "h");

                        let expEdgeCount = 1;
                        let expEdgeAtG = 2;  // index of "h"
                        let expEdgeAtH = 1;  // index of "g"

                        //* act *//
                        graph.addEdge("g", "h");

                        //* assemble *//
                        let acAtG = graph.edges[1];
                        let acAtH = graph.edges[2];

                        //* assert *//
                        this.kit.equate(expEdgeCount, acAtG.length);
                        this.kit.equate(expEdgeCount, acAtH.length);

                        this.kit.equate(expEdgeAtG, acAtG[0]);
                        this.kit.equate(expEdgeAtH, acAtH[0]);
                    }
                );

                this.kit.test("addEdge",
                    "When other edges exist, a new edge is added in correct order for each vertex.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph, "f", "g", "h");

                        // edges are vertex indices; using some nonexistent ones here
                        graph.edges = [/* f: */ [0, 3], /* g: */ [6, 8, 9], /* h: */ [0, 4, 12, 16]];
                        let expecteds = [/* f: */ [0, 3], /* g: */ [2, 6, 8, 9], /* h: */ [0, 1, 4, 12, 16]];

                        let expAtG = expecteds[1];
                        let expAtH = expecteds[2];

                        //* act *//
                        graph.addEdge("g", "h");

                        //* assemble *//
                        let acAtG = graph.edges[1];
                        let acAtH = graph.edges[2];

                        //* assert *//
                        // after insertions, edge arrays for each
                        // vertex should both be one longer
                        this.kit.equate(expAtG.length, acAtG.length);
                        this.kit.equate(expAtH.length, acAtH.length);

                        // elements of edge array for first vertex
                        for (let of = 0; of < expAtG.length; of++) {
                            this.kit.equate(expAtG[of], acAtG[of]);
                        }

                        // elements of edge array for second vertex
                        for (let of = 0; of < expAtH.length; of++) {
                            this.kit.equate(expAtH[of], acAtH[of]);
                        }
                    }
                );

                this.kit.test("addEdge",
                    "When edge to be added already exists, edge arrays are not changed.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph, "f", "g", "h");

                        // edges are vertex indices; the "new" edge
                        // already exists in its vertices' arrays;
                        // also using some nonexistent indices here
                        graph.edges = [/* f: */ [0, 3], /* g: */ [2, 6, 8, 9], /* h: */ [0, 1, 4, 12, 16]];
                        let expecteds = [/* f: */ [0, 3], /* g: */ [2, 6, 8, 9], /* h: */ [0, 1, 4, 12, 16]];

                        let expAtG = expecteds[1];
                        let expAtH = expecteds[2];

                        //* act *//
                        graph.addEdge("g", "h");

                        //* assemble *//
                        let acAtG = graph.edges[1];
                        let acAtH = graph.edges[2];

                        //* assert *//
                        // after insertions, edge arrays for each
                        // vertex should both be one longer
                        this.kit.equate(expAtG.length, acAtG.length);
                        this.kit.equate(expAtH.length, acAtH.length);

                        // elements of edge array for first vertex
                        for (let of = 0; of < expAtG.length; of++) {
                            this.kit.equate(expAtG[of], acAtG[of]);
                        }

                        // elements of edge array for second vertex
                        for (let of = 0; of < expAtH.length; of++) {
                            this.kit.equate(expAtH[of], acAtH[of]);
                        }
                    }
                );

                // endregion addEdge()

                // region findVertex()

                this.kit.test("findVertex",
                    "When sought vertex doesn't exist, null is returned.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph, "a", "b", "c", "d", "e", "f", "g", "h");
                        graph.addEdge("a", "d");
                        graph.addEdge("d", "h");

                        let expected = null;

                        //* act *//
                        let actual = graph.findVertex("q");

                        //* assert *//
                        this.kit.equate(expected, actual);
                    }
                );

                this.kit.test("findVertex",
                    "When sought vertex exists, its index and edges are returned.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph, "a", "b", "c", "d", "e", "f", "g", "h");
                        graph.addEdge("a", "d");
                        graph.addEdge("d", "h");

                        // "d" is at 3 and has edges to "a" at 0 and "h" at 7
                        let expected = {index: 3, edges: [0, 7]};

                        //* act *//
                        let actual = graph.findVertex("d");

                        //* assert *//
                        this.kit.equate(expected.index, actual.index);
                        this.kit.equate(expected.edges.length, actual.edges.length);

                        for (let of = 0; of < expected.edges.length; of++) {
                            this.kit.equate(expected.edges[of], actual.edges[of]);
                        }
                    }
                );

                // endregion findVertex()

                // region findVertices()

                this.kit.test("findVertices",
                    "When vertices are sought by two other vertices, the correct vertices are returned.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph, "a", "b", "c", "d", "e", "f", "g", "h");

                        // b neighbors a, f, and h; e neighbors a and h;
                        // vertices a and h are neighbors of both b and e
                        this.addGraphEdges(graph, {from: "b", to: "a"}, {from: "b", to: "f"}, {from: "b", to: "h"});
                        this.addGraphEdges(graph, {from: "e", to: "a"}, {from: "e", to: "h"});

                        let expecteds = [{name: "a", edges: [1, 4]}, {name: "h", edges: [1, 4]}];

                        //* act *//
                        let actuals = graph.findVertices("b", "e");

                        //* assert *//
                        // the right number of vertices should have been returned
                        this.kit.equate(expecteds.length, actuals.length);

                        // each of the vertices returned should have the right name and edges
                        for (let of = 0; of < expecteds.length; of++) {
                            // name
                            this.kit.equate(expecteds[of].name, actuals[of].name);
                            this.kit.equate(expecteds[of].edges.length, actuals[of].edges.length);

                            // localizing edges
                            let expEdges = expecteds[of].edges;
                            let acEdges = actuals[of].edges;

                            // edges
                            for (let at = 0; at < expEdges.length; at++) {
                                this.kit.equate(expEdges[at], acEdges[at]);
                            }
                        }
                    }
                );


                this.kit.test("findVertices",
                    "When vertices are sought by three other vertices, the correct vertices are returned.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph,
                            "a", "b", "c", "d", "e", "f", "g",
                            "h", "i", "j", "k", "l", "m"
                        );

                        // b neighbors a, f, g, and h
                        this.addGraphEdges(graph,
                            {from: "b", to: "a"}, {from: "b", to: "f"},
                            {from: "b", to: "g"}, {from: "b", to: "h"}
                        );

                        // e neighbors a, c, g, and h
                        this.addGraphEdges(graph,
                            {from: "e", to: "a"}, {from: "e", to: "c"},
                            {from: "e", to: "g"}, {from: "e", to: "h"}
                        );

                        // d neighbors a, and g
                        graph.addEdge("d", "a");
                        graph.addEdge("d", "g");

                        // a few extra neighbors for fuller edge arrays in output, including between the founds
                        this.addGraphEdges(graph,
                            {from: "a", to: "j"}, {from: "g", to: "l"},
                            {from: "a", to: "k"}, {from: "g", to: "m"},
                            {from: "a", to: "i"}, {from: "a", to: "g"}
                        );

                        // a and g are the only neighbors of all of b, e, and d;
                        // their edges include all their neighbors, sought or not
                        let expecteds = [
                            {name: "a", edges: [1, 3, 4, 6, 8, 9, 10]},
                            {name: "g", edges: [0, 1, 3, 4, 11, 12]}
                        ];

                        //* act *//
                        let actuals = graph.findVertices("e", "d", "b");

                        //* assert *//
                        // the right number of vertices should have been returned
                        this.kit.equate(expecteds.length, actuals.length);

                        // each of the vertices returned should have the right name and edges
                        for (let of = 0; of < expecteds.length; of++) {
                            // name
                            this.kit.equate(expecteds[of].name, actuals[of].name);
                            this.kit.equate(expecteds[of].edges.length, actuals[of].edges.length);

                            // localizing edges
                            let expEdges = expecteds[of].edges;
                            let acEdges = actuals[of].edges;

                            // edges
                            for (let at = 0; at < expEdges.length; at++) {
                                this.kit.equate(expEdges[at], acEdges[at]);
                            }
                        }
                    }
                );

                // endregion findVertices()

                // region removeEdge()

                this.kit.test("removeEdge",
                    "When an edge is removed, it is no longer found in either vertex's .edges array.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph, "a", "b", "c", "d");
                        graph.addEdge("a", "b");
                        graph.addEdge("a", "c");
                        graph.addEdge("a", "d");
                        graph.addEdge("b", "d");

                        let expEdgesFromA = [1, 2];
                        let expEdgesFromD = [1];

                        //* act *//
                        graph.removeEdge("a", "d");

                        //* assert *//
                        this.kit.equate(expEdgesFromA.length, graph.edges[0].length);

                        for (let at = 0; at < expEdgesFromA.length; at++) {
                            let expected = expEdgesFromA[at];
                            let actual = graph.edges[0][at];

                            this.kit.equate(expected, actual);
                        }

                        this.kit.equate(expEdgesFromD.length, graph.edges[3].length);

                        for (let at = 0; at < expEdgesFromD.length; at++) {
                            let expected = expEdgesFromD[at];
                            let actual = graph.edges[3][at];

                            this.kit.equate(expected, actual);
                        }
                    }
                );

                // endregion removeEdge()

                // region removeVertex()

                this.kit.test("removeVertex",
                    "When a vertex is removed, it is no longer present in the graph's .vertices array.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph, "a", "b", "c", "d");
                        this.addGraphEdges(graph, {from: "a", to: "c"}, {from: "b", to: "d"});

                        let expecteds = ["a", "c", "d"];

                        //* act *//
                        graph.removeVertex("b");

                        //* assert *//
                        this.kit.equate(expecteds.length, graph.vertices.length);

                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = graph.vertices[at];

                            this.kit.equate(expected, actual);
                        }
                    }
                );

                this.kit.test("removeVertex",
                    "When a vertex is removed, edges to and from it are also removed.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph, "a", "b", "c", "d");
                        this.addGraphEdges(
                            graph,
                            {from: "a", to: "c"},
                            {from: "b", to: "c"},
                            {from: "b", to: "a"},
                            {from: "d", to: "b"},
                            {from: "c", to: "d"}
                        );

                        let expAtA = [1, 2];
                        let expAtB = [0, 2];
                        let expAtC = [0, 1];

                        //* act *//
                        // removing the last vertex so that there
                        // are no later indices to be changed here
                        graph.removeVertex("d");

                        //* assert *//
                        this.kit.equate(expAtA.length, graph.edges[0].length);
                        this.kit.equate(expAtB.length, graph.edges[1].length);
                        this.kit.equate(expAtC.length, graph.edges[2].length);

                        let expecteds = [expAtA, expAtB, expAtC];
                        let actuals = [graph.edges[0], graph.edges[1], graph.edges[2]];

                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];

                            for (let of = 0; of < expected.length; of++) {
                                this.kit.equate(expected[of], actual[of]);
                            }
                        }
                    }
                );

                this.kit.test("removeVertex",
                    "When a vertex is removed, any edge to a higher-index vertex is decremented by one.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph, "a", "b", "c", "d", "e");
                        graph.addEdge("a", "e");

                        let expAtA = [3];
                        let expAtE = [0];

                        //* act *//
                        // removing the last vertex so that there
                        // are no later indices to be changed here
                        graph.removeVertex("b");

                        //* assert *//
                        this.kit.equate(expAtA.length, graph.edges[0].length);
                        this.kit.equate(expAtE.length, graph.edges[3].length);

                        let expecteds = [expAtA, expAtE];
                        let actuals = [graph.edges[0], graph.edges[3]];

                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];

                            for (let of = 0; of < expected.length; of++) {
                                this.kit.equate(expected[of], actual[of]);
                            }
                        }
                    }
                );

                this.kit.test("removeVertex",
                    "When a vertex is removed, only edges to remaining vertices exist, and they retain the correct indices.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph, "a", "b", "c", "d", "e");
                        this.addGraphEdges(
                            graph,
                            {from: "a", to: "c"},
                            {from: "a", to: "b"},
                            {from: "a", to: "d"},
                            {from: "a", to: "e"},
                            {from: "b", to: "c"},
                            {from: "b", to: "d"},
                            {from: "b", to: "e"},
                            {from: "c", to: "d"},
                            {from: "c", to: "e"}
                        );

                        let expAtA = [1, 2, 3];
                        let expAtC = [0, 2, 3];
                        let expAtD = [0, 1];
                        let expAtE = [0, 1];

                        //* act *//
                        // removing the last vertex so that there
                        // are no later indices to be changed here
                        graph.removeVertex("b");

                        //* assert *//
                        this.kit.equate(expAtA.length, graph.edges[0].length);
                        this.kit.equate(expAtC.length, graph.edges[1].length);
                        this.kit.equate(expAtD.length, graph.edges[2].length);
                        this.kit.equate(expAtE.length, graph.edges[3].length);

                        let expecteds = [
                            expAtA, expAtC, expAtD, expAtE
                        ];
                        let actuals = [
                            graph.edges[0], graph.edges[1],
                            graph.edges[2], graph.edges[3]
                        ];

                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];

                            for (let of = 0; of < expected.length; of++) {
                                this.kit.equate(expected[of], actual[of]);
                            }
                        }
                    }
                );

                // endregion removeVertex()

            }
        );
    }

    // endregion tests


    // region fixtures

    addGraphVertices(graph, ...vertices) {
        for (let vertex of vertices) {
            graph.addVertex(vertex);
        }
    }

    addGraphEdges(graph, ...edges) {
        for (let {from, to} of edges) {
            graph.addEdge(from, to);
        }
    }

    equateArrays(firstArray, secondArray) {
        // equal arrays have the same lengths
        this.kit.equate(firstArray.length, secondArray.length);

        // equal arrays have the same elements at each index
        for (let at = 0; at < firstArray.length; at++) {
            this.kit.equate(firstArray[at], secondArray[at]);
        }
    }

    // endregion fixtures
}
